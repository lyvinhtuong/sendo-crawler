<?php
include('vendor/autoload.php');

$country = "VN";

$parameters = new \Protobuf\Identity\Crawler\Core\Parameters();
$parameters->setStartDate("");
$parameters->setEndDate("");

$jobRequest = new \Protobuf\Identity\Crawler\Core\JobRequest();
$jobRequest->setCountry($country);
$jobRequest->setDataPoint(\Epsilo\Crawler\SendoChannel::KWBD_KEYWORD_PERFORMANCE);
$jobRequest->setCookie('');
$jobRequest->setParameters($parameters);

$crawler = new \Epsilo\Crawler\SendoChannel();
$jobResponse = $crawler->GetData($jobRequest);
var_dump($jobResponse);